import { initializeApp } from 'firebase/app';

import { getFirestore } from "firebase/firestore"

const firebaseConfig = {

    apiKey: "AIzaSyB7LEKcUj5nI9A-m_8c81duoQAc-XVIoMQ",
  
    authDomain: "remind-it-34.firebaseapp.com",
  
    projectId: "remind-it-34",
  
    storageBucket: "remind-it-34.appspot.com",
  
    messagingSenderId: "286478772552",
  
    appId: "1:286478772552:web:7d1ba3d3acedd58f02ab7d"
  
  };
  

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);

  // const timestamp = firebase.firestore.FieldValue.serverTimestamp;
  const db = getFirestore();

  // export { timestamp }
  export { db }